Source: dyssol
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Anton Gladky <gladk@debian.org>
Section: science
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 13),
               libgraphviz-dev,
               libhdf5-dev,
               libsundials-dev,
               libqt5opengl5-dev,
               zlib1g-dev
Build-Depends-Indep: python3-sphinx,
                     python3-sphinx-rtd-theme,
                     texlive-xetex,
                     tex-gyre,
                     texlive-fonts-recommended
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/dyssol
Vcs-Git: https://salsa.debian.org/science-team/dyssol.git
Homepage: https://github.com/FlowsheetSimulation/Dyssol-open

Package: dyssol
Architecture: any
Multi-Arch: foreign
Depends: libdyssol1.0t64 (= ${binary:Version}),
         dyssol-data,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Tool for dynamic flowsheet simulation. Command line tool
 Dynamic simulation of solids processes, is a dynamic flowsheet modelling system
 designed to simulate the time-dependent behaviour  of complex production
 processes in solids processing technology
 .
 This package contains dyssol`s command line tool.

Package: dyssol-gui
Architecture: any
Multi-Arch: foreign
Depends: libdyssol1.0t64 (= ${binary:Version}),
         dyssol-data,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Tool for dynamic flowsheet simulation. GUI
 Dynamic simulation of solids processes, is a dynamic flowsheet modelling system
 designed to simulate the time-dependent behaviour  of complex production
 processes in solids processing technology
 .
 This package contains dyssol`s GUI.

Package: libdyssol-dev
Architecture: any
Section: libdevel
Depends: libdyssol1.0t64 (= ${binary:Version}),
         ${misc:Depends}
Description: Tool for dynamic flowsheet simulation. Development files nad headers
 Dynamic simulation of solids processes, is a dynamic flowsheet modelling system
 designed to simulate the time-dependent behaviour  of complex production
 processes in solids processing technology
 .
 This package contains dyssol`s development files nad headers

Package: libdyssol1.0t64
Provides: ${t64:Provides}
Replaces: libdyssol1.0
Breaks: libdyssol1.0 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Tool for dynamic flowsheet simulation. Shared libraries
 Dynamic simulation of solids processes, is a dynamic flowsheet modelling system
 designed to simulate the time-dependent behaviour  of complex production
 processes in solids processing technology
 .
 This package contains dyssol`s shared libraries.

Package: dyssol-data
Architecture: all
Depends: ${misc:Depends}
Recommends: dyssol
Description: Tool for dynamic flowsheet simulation. Data files
 Dynamic simulation of solids processes, is a dynamic flowsheet modelling system
 designed to simulate the time-dependent behaviour  of complex production
 processes in solids processing technology
 .
 This package contains examples and data files.

Package: dyssol-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends},
         ${misc:Depends},
         libjs-mathjax
Recommends: dyssol
Description: Tool for dynamic flowsheet simulation. Documentation
 Dynamic simulation of solids processes, is a dynamic flowsheet modelling system
 designed to simulate the time-dependent behaviour  of complex production
 processes in solids processing technology
 .
 This package contains documentation.
